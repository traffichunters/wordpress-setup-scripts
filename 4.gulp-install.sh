#!/bin/bash
sudo apt-get install npm
sudo npm install gulp -g
sudo apt-get update
sudo apt-get install ruby-full rubygems
sudo gem install sass

echo "Enter name of project":
read filename
echo "Enter name of theme":
read themename
cd ~/Documents/$filename/wp-content/themes/$themename
sudo npm install gulp --save-dev
sudo npm install gulp-ruby-sass gulp-autoprefixer gulp-minify-css gulp-rename --save-dev
sudo npm install
